# README #

Anonymous Browsing made easy! TOR & I2P Client

### Application overview ###

![darknet.png](https://bitbucket.org/repo/oL75kM6/images/1798937357-darknet.png)
![darknet2.png](https://bitbucket.org/repo/oL75kM6/images/3475193451-darknet2.png)
![darknet3.png](https://bitbucket.org/repo/oL75kM6/images/2225458182-darknet3.png)

### How to install: ###

git clone https://eiNjel@bitbucket.org/eiNjel/d4rkn3t.git

cd d4rkn3t && tar xvf d4rkn3t-0.6.8.tar.gz

For Arch Linux and its derivates use:

makepkg -sci

Bad check sums?

updpkgsums

Other Linux Debian, RHEL and its derivates (Ubuntu, Mint, Fedora, CentOS etc):

sudo sh setup.sh

Other linux distributions are not yet supported.
